"use strict";

function Ship(size, name, sign) {
  this.x = 0;
  this.y = 0;
  this.size = size;
  this.hits = 0;
  this.horizontal = Math.random() < 0.5;
  this.sign = sign;
  this.name = name;
  return this;
}


module.exports = Ship;
const config = {
  board: {
    boardRows: 10, //ось Y Ordinate
    boardCols: 10  //ось X Abscissa
  },
  signs: {
    BORDER: 'o',
    WATER: ' ',
    SHIP: '#',
    HIT: '*'
  },
  game: {
    allowTouchBorder: false,
    status: {
      IN_PROGRESS : 1,
      GAME_OVER : 2
    }
  },
  ships: {
    Carrier: {sign: '#', deck: 4, count: 1},
    Cruiser: {sign: '#', deck: 3, count: 2},
    Submarine: {sign: '#', deck: 2, count: 3},
    Destroyer: {sign: '#', deck: 1, count: 4}
  }

}
module.exports = config;
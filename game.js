var Player = require('./player.js');
const config = require('./config.js');
const IN_PROGRESS = config.game.status.IN_PROGRESS;
const GAME_OVER = config.game.status.GAME_OVER;


function Game(id, player, opponent) {
  this.id = id;
  this.currentPlayer = Math.floor(Math.random() * 2);
  this.winningPlayer = null;
  this.gameStatus = IN_PROGRESS;

  this.players = [new Player(player), new Player(opponent)];
}

Game.prototype.getPlayerId = function(player) {
  return this.players[player].id;
};


Game.prototype.getWinnerId = function() {
  if(this.winningPlayer === null) {
    return null;
  }
  return this.players[this.winningPlayer].id;
};


Game.prototype.getLoserId = function() {
  if(this.winningPlayer === null) {
    return null;
  }
  var loser = this.winningPlayer === 0 ? 1 : 0;
  return this.players[loser].id;
};


Game.prototype.switchPlayer = function() {
  this.currentPlayer = this.currentPlayer === 0 ? 1 : 0;
};


Game.prototype.abortGame = function(player) {
  this.gameStatus = GAME_OVER;
  this.winningPlayer = player === 0 ? 1 : 0;
}

/**
 * @param x
 * @param y
 * @returns {boolean}
 */
Game.prototype.shoot = function(x,y) {
  var opponent = this.currentPlayer === 0 ? 1 : 0;
  //если в клетку еще не стреляли и игра не закончена
  if(this.players[opponent].shots[x][y] === 0 && this.gameStatus === IN_PROGRESS) {
    // Square has not been shot at yet.
    if(!this.players[opponent].shoot(x,y)) {
      //Если промах, переключить игрока
      this.switchPlayer();
      return false;
    }

    //Если все палубы затоплены то конец игры
    if(this.players[opponent].decks <= 0) {
      this.gameStatus = GAME_OVER;
      this.winningPlayer = opponent === 0 ? 1 : 0;
    }

    return true;
  }

  return false;
};

Game.prototype.checkGameOver = function () {
   return this.gameStatus === GAME_OVER;
}

module.exports = Game;

"use strict";
const config = require('./config.js');
const Board = require('./Board.js');
const Ship = require('./Ship.js');

function Player (id) {
  // var i;

  this.id = id;
  //история выстрелов каждого игрока
  this.shots = [];
  //игровая доска
  this.shipGrid = [];
  //корабли игрока
  this.ships = [];
  this.decks = 0;

  let board = new Board(config, config.board.boardCols, config.board.boardRows);
  this.setBoard(board);
  this.initBoard();
  this.initShipsOnBoard();

  // Board.print(this.shipGrid);

}


Player.prototype.shoot = function(x, y) {

  if(this.shots[x][y] > 0){
    //уже был выстрел в эту клетку
    return true;
  }
  this.shots[x][y] = 1;

  if(this.shipGrid[x][y] === config.signs.SHIP) {
    //Попадание
    //уменьшаем общее количество палуб
    this.decks--;
    return true;
  } else {
    // Промах
    return false;
  }

};



Player.prototype.setShipOnBoard = function (place) {
  for (let x in place) {
    if(place.hasOwnProperty(x))
    for (let y in place[x]) {
      if(place[x].hasOwnProperty(y))
      this.shipGrid[x][y] = place[x][y];
    }
  }
  Board.print(this.shipGrid);

}

//ф-ция рекурсивная, пытается найти место для корабля что бы он не соприкасался с другими
Player.prototype.checkPlaceShip = function (ship) {
  let tryArray = [];
  let offset;
  if (ship.horizontal) {
    //если корабль расположен горизонтально то нужно что бы он не выступал за правую сторону доски
    let xMax = this.board.getAbscissa() - ship.size + 1;
    //выбрать произвольную координату так что бы корабль поместился
    ship.x = Math.floor(Math.random() * xMax);
    ship.y = Math.floor(Math.random() * this.board.getOrdinate());

    for (let x = 0; x < ship.size; x++) {
      offset = ship.x + x;
      if(this.shipGrid[offset][ship.y] === config.signs.WATER){
        tryArray[offset] = [];
        tryArray[offset][ship.y] = ship.sign
      }else{
        return this.checkPlaceShip(ship);
      }
    }

  } else {
    //если корабль расположен вертикально то нужно что бы он не выступал за нижнюю сторону доски
    let yMax = this.board.getOrdinate() - ship.size + 1;
    //выбрать произвольную координату так что бы корабль поместился
    ship.y = Math.floor(Math.random() * yMax);
    ship.x = Math.floor(Math.random() * this.board.getAbscissa());

    tryArray[ship.x] = [];
    for (let y = 0; y < ship.size; y++) {
      offset = ship.y + y;
      if(this.shipGrid[ship.x][offset] === config.signs.WATER){
        tryArray[ship.x][offset] = ship.sign
      }else{
        return this.checkPlaceShip(ship);
      }
    }
  }


  return tryArray;


}




//наполняем , проверяем не выходят ли координаты за игровую доску
//если нет то делаем смещение по оси y от корабля, вверх и вниз
//счетчки с -1 потому что нужно взять впереди и позади корабля
//по правилам нельзя что бы корабли соприкасались углами
//[ ][ ][ ][w][w][w][w][w][w][w]
//[ ][ ][ ][w][#][#][#][#][#][w]
//[ ][ ][ ][w][w][w][w][w][w][w]
Player.prototype.fillWaterAroundShip = function (ship) {
  let offset;

  if (ship.horizontal) {

    for (let x = -1; x <= ship.size; x++) {
      offset = ship.x + x;
      //наполняем ниже корабля, ось Y-1, ось X по счетчику
      if (this.shipGrid[offset] && this.shipGrid[offset][ship.y - 1]) {
        this.shipGrid[offset][ship.y - 1] = config.signs.BORDER;
      }
      //наполняем выше корабля, ось Y+1, ось X по счетчику
      if (this.shipGrid[offset] && this.shipGrid[offset][ship.y + 1]) {
        this.shipGrid[offset][ship.y + 1] = config.signs.BORDER;
      }
    }

    //наполняем фронт, клетка за кораблем, горизонтально самая правая
    offset = ship.x + ship.size;
    if (this.shipGrid[offset] && this.shipGrid[offset][ship.y]) {
      this.shipGrid[offset][ship.y] = config.signs.BORDER;
    }
    //наполняем тым, клетка перед кораблем, горизонтально самая левая
    offset = ship.x - 1;
    if (this.shipGrid[offset] && this.shipGrid[offset][ship.y]) {
      this.shipGrid[offset][ship.y] = config.signs.BORDER;
    }
  } else {
    for (let y = -1; y <= ship.size; y++) {
      offset = ship.y + y;
      //наполняем возле левого борта, ось X-1, ось Y по счетчику, сместились влево и заполняем вертикально вниз
      if (this.shipGrid[ship.x - 1] && this.shipGrid[ship.x - 1][offset]) {
        this.shipGrid[ship.x - 1][offset] = config.signs.BORDER;
      }
      //наполняем возле правого борта, ось Y+1, ось X по счетчику, сместились вправо и заполняем вертикально вниз
      if (this.shipGrid[ship.x + 1] && this.shipGrid[ship.x + 1][offset]) {
        this.shipGrid[ship.x + 1][offset] = config.signs.BORDER;
      }
    }

    //наполняем фронт
    offset = ship.y + ship.size;
    if (this.shipGrid[ship.x] && this.shipGrid[ship.x][offset]) {
      this.shipGrid[ship.x][offset] = config.signs.BORDER;
    }
    //наполняем тыл
    offset = ship.y - 1;
    if (this.shipGrid[ship.x] && this.shipGrid[ship.x][offset]) {
      this.shipGrid[ship.x][offset] = config.signs.BORDER;
    }
  }
  console.log('');
  Board.print(this.shipGrid);

}

Player.prototype.setBoard = function setBoard (board) {
  this.board = board;
}

Player.prototype.initShipsOnBoard = function (board) {
  for(let ship in config.ships){
    if(config.ships.hasOwnProperty(ship))
      for(let i = 0; i < config.ships[ship].count; i++){
        let boat = new Ship(config.ships[ship].deck , ship , config.ships[ship].sign);
        this.decks += config.ships[ship].deck;

        let place =  this.checkPlaceShip(boat);
        place =  this.setShipOnBoard(place);
        this.fillWaterAroundShip(boat);

        this.ships.push(boat)
      }
  }
}


Player.prototype.initBoard = function (board) {
  //координаты как у точки в декартовой системе (x,y)
  let abscissa = this.board.getAbscissa();
  let ordinate = this.board.getOrdinate();
  for (let x = 0; x < abscissa; x++) {
    this.shipGrid[x] = [];
    this.shots[x] = [];
    for (let y = 0; y < ordinate; y++) {
      //инициализируем игровую доску
      this.shipGrid[x][y] = ' ';
      //инициализируем массив для истории выстрелов
      this.shots[x][y] = 0;
    }
  }
}


module.exports = Player;



















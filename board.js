"use strict";
function Board(cfg, abscissa, ordinate){

  this.getAbscissa = function () {
    return abscissa;
  }

  this.getOrdinate = function () {
    return ordinate;
  }
}


Board.print = function(grid) {
  var append;
  var str = "";
  var hr = '------------------------------';
  console.log(hr);

  function transpose(a) {
    return Object.keys(a[0]).map(function(c) {
      return a.map(function(r) { return r[c]; });
    });
  }

  let tr_grid =  transpose(grid);

  for(let row in tr_grid){
    str = '';
    if(tr_grid.hasOwnProperty(row))
    for (let col in tr_grid[row]){
      if(tr_grid[row].hasOwnProperty(col))
      str = str + "[" + tr_grid[row][col] + "]";
    }
    console.log(str);
  }

  console.log(hr);
};

module.exports = Board;
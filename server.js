var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var Game = require('./game.js');

var port = 8903;

var users = {};
var gameCounter = 1;


http.listen(port, function(){
  console.log('listening on *:' + port);
});

io.on('connection', function(socket) {
  console.log((new Date().toISOString()) + ' ID ' + socket.id + ' connected.');

  users[socket.id] = {
    game: null,
    player: null
  };

  //сначала добавить клиента в комнату, пока не присоедениться опонент
  socket.join('room');

  // position {x,y}
  socket.on('shot', function(position) {
    var game = users[socket.id].game, opponent;

    if(game !== null) {
      // проверить кто стреляет, чья очередь
      if(game.currentPlayer === users[socket.id].player) {
        opponent = game.currentPlayer === 0 ? 1 : 0;

        if(game.shoot(position.x, position.y)) {

          if(game.checkGameOver()){
            io.to(users[socket.id].game.getWinnerId()).emit('gameover', true);
            io.to(users[socket.id].game.getLoserId()).emit('gameover', false);
          }

          //отправить свою дуску и доску для стрельбы
          io.to(socket.id).emit('update', {shipGrid : game.players[!opponent].shipGrid, shots: game.players[!opponent].shots});
          io.to(game.getPlayerId(opponent)).emit('update', {shipGrid : game.players[opponent].shipGrid, shots: game.players[opponent].shots});
        }
      }
    }
  });


  socket.on('leave', function() {
    if(users[socket.id].game !== null) {
      leaveGame(socket);
      socket.join('room');
      joinWaitingPlayers();
    }
  });


  socket.on('disconnect', function() {
    console.log((new Date().toISOString()) + ' ID ' + socket.id + ' disconnected.');

    leaveGame(socket);

    delete users[socket.id];
  });

  joinWaitingPlayers();
});

function joinWaitingPlayers() {
  var clients = getClient('room');

  if(clients.length >= 2) {
    //если больше одного игрока начинаем игру
    var game = new Game(gameCounter++, clients[0].id, clients[1].id);

    //покинуть временную комнату, создать новую
    clients[0].leave('room');
    clients[1].leave('room');
    clients[0].join('game' + game.id);
    clients[1].join('game' + game.id);

    users[clients[0].id].player = 0;
    users[clients[1].id].player = 1;
    users[clients[0].id].game = game;
    users[clients[1].id].game = game;

    io.to('game' + game.id).emit('join', game.id);

    //отправить свою дуску и доску для стрельбы
    io.to(clients[0].id).emit('update', {shipGrid : game.players[0].shipGrid, shots: game.players[0].shots});
    io.to(clients[1].id).emit('update', {shipGrid : game.players[0].shipGrid, shots: game.players[0].shots});

    console.log((new Date().toISOString()) + " " + clients[0].id + " and " + clients[1].id + " have joined game ID " + game.id);
  }
}

function leaveGame(socket) {
  if(users[socket.id].game !== null) {
    console.log((new Date().toISOString()) + ' ID ' + socket.id + ' left game ID ' + users[socket.id].game.id);

    // уведомить играка что опонент покинул игру
    socket.broadcast.to('game' + users[socket.id].game.id).emit('notification', {
      message: 'уведомить играка что опонент покинул игру'
    });

    if (!users[socket.id].game.checkGameOver()) {
      //Игра еще не закончилась но нужно ее прекратить
      users[socket.id].game.abortGame(users[socket.id].player);

      io.to(users[socket.id].game.getWinnerId()).emit('gameover', true);
      io.to(users[socket.id].game.getLoserId()).emit('gameover', false);
    }

    socket.leave('game' + users[socket.id].game.id);

    users[socket.id].game = null;
    users[socket.id].player = null;

    io.to(socket.id).emit('leave');
  }
}



function getClient(room) {
  var clients = [];
  for (var id in io.sockets.adapter.rooms[room]) {
    clients.push(io.sockets.adapter.nsp.connected[id]);
  }
  return clients;
}


